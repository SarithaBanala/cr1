com.conformiq.creator.structure.v15
creator.gui.screen GUIExcelImportShopping32Application32UIParabankLogin
"Parabank|Login"
{
	creator.gui.form GUIExcelImportShopping32Application32UIParabankLoginForm
	"Parabank|Login"
	{
		creator.gui.textbox
		GUIExcelImportShopping32Application32UIParabankLoginUser32Name "User Name"
			type = String
			status = dontcare;
		creator.gui.textbox
		GUIExcelImportShopping32Application32UIParabankLoginPassword "Password"
			type = String
			status = dontcare;
		creator.gui.textbox
		GUIExcelImportShopping32Application32UIParabankLoginEmail "Email"
			type = String
			status = dontcare;
	}
	creator.gui.button
	GUIExcelImportShopping32Application32UIParabankLoginSign32in "Sign in"
		status = dontcare;
}
creator.gui.screen GUIExcelImportShopping32Application32UIParabankHome
"Parabank|Home"
{
	creator.gui.hyperlink
	GUIExcelImportShopping32Application32UIParabankHomeOpen32New32Account
	"Open New Account"
		status = dontcare;
	creator.gui.hyperlink
	GUIExcelImportShopping32Application32UIParabankHomeTransfer32Funds
	"Transfer Funds"
		status = dontcare;
}
creator.gui.screen GUIExcelImportShopping32Application32UIShopping32AppMain
"Shopping App|Main"
{
	creator.gui.form GUIExcelImportShopping32Application32UIShopping32AppMainForm
	"Shopping App|Main"
	{
		creator.gui.textbox
		GUIExcelImportShopping32Application32UIShopping32AppMainQuantity "Quantity"
			type = number
			status = dontcare;
	}
	creator.gui.button
	GUIExcelImportShopping32Application32UIShopping32AppMainAdd32Item "Add Item"
		status = dontcare;
	creator.gui.button
	GUIExcelImportShopping32Application32UIShopping32AppMainCheckout "Checkout"
		status = dontcare;
	creator.gui.labelwidget
	GUIExcelImportShopping32Application32UIShopping32AppMainSKU32Error
	"SKU Error"
		status = dontcare;
	creator.gui.labelwidget
	GUIExcelImportShopping32Application32UIShopping32AppMainQuantity32Error
	"Quantity Error"
		status = dontcare;
}